package marc.sabina.lab12.ex3;

import java.util.ArrayList;
import java.util.Collections;

public class Parking {

    ArrayList<Vehicle> parkedVechicles = new ArrayList<Vehicle>();

    public void parkVehicle(Vehicle e) {
        parkedVechicles.add(e);

    }

    /**
     * Sort vehicles by length.
     */
    public void sortByWeight() {
        Collections.sort(parkedVechicles);
    }

    public Vehicle get(int index) {
        return parkedVechicles.get(index);
    }

}