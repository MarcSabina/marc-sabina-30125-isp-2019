package marc.sabina.lab12.ex3;

public class ElectricVehicle extends Vehicle {
    public ElectricVehicle(String type, int length) {
        super(type, length);
    }

    @Override
    public String start() {
        return "electric engine started";
    }
}
