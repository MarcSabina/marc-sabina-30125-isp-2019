package marc.sabina.lab12.ex3;

public class ElectricBattery {

    /**
     * Percentage load.
     */
    static int charge = 0;

    ElectricBattery() throws BatteryException {

        charge++;
        if (charge > 100) throw new BatteryException("full charge", charge);
    }

    public void charge() {
        charge++;
    }

}