package marc.sabina.lab12.ex3;


public class Vehicle implements Comparable<Vehicle> {

    private String type;
    private int length;

    public Vehicle(String type, int length) {
        this.type = type;
        this.length = length;
    }

    public String start() {
        return "engine started";
    }

    public int getLength() {
        return length;
    }

    public int compareTo(Vehicle o) {
        return (this.getLength() < o.getLength() ? -1 :
                (this.getLength() == o.getLength() ? 0 : 1));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Vehicle)) {
            return false;
        }
        Vehicle v = (Vehicle) obj;

        return v.length == length && v.type.equals(type);
    }
}
