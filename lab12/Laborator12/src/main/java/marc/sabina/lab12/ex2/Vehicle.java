package marc.sabina.lab12.ex2;

import java.util.Objects;

public class Vehicle implements Comparable<Vehicle> {
    private String type;
    private int weight;

    public Vehicle(String type, int weight) {
        this.type = type;
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return weight == vehicle.weight &&
                type.equals(vehicle.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, weight);
    }

    public int compareTo(Vehicle o) {
        return (this.getWeight() < o.getWeight() ? -1 :
                (this.getWeight() == o.getWeight() ? 0 :1));
    }
}

