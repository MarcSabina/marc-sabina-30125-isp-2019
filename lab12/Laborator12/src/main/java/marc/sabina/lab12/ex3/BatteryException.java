package marc.sabina.lab12.ex3;

public class BatteryException extends Exception {

    int charge;

    public BatteryException(String msg, int charge) {
        super(msg);
        this.charge = charge;
    }

    public int getCharge() {
        return charge;
    }
}
