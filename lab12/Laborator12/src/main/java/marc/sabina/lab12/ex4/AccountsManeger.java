package marc.sabina.lab12.ex4;

import java.util.ArrayList;

public class AccountsManeger {

   ArrayList<BankAccount> am = new ArrayList<BankAccount>();

    public void addAccount(BankAccount account) {
        am.add(account);

    }

    public boolean exists(String id) {
        int k = 0;

        for (BankAccount temp : am) {
            if (temp.getId() == id)
                k++;
        }
        if (k != 0) return true;
        return false;
    }

    public int count() {

        return am.size();
    }
}