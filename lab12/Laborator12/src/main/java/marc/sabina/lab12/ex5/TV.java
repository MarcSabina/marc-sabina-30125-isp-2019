package marc.sabina.lab12.ex5;

public class TV extends ElectronicDevice {
    int channel;

    public TV() {
    }

    void channelUp() {
        if (channel <= 10) {
            channel++;
            System.out.println("Channel up " + channel);
        }
    }

    void channelDown() {
        if (channel >= 2) {
            channel--;
            System.out.println("Channel down " + channel);
        }
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }
}