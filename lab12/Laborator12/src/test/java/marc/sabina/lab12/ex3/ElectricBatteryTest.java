package marc.sabina.lab12.ex3;

import org.junit.Test;

public class ElectricBatteryTest {

    /**
     * Expect battery to throw excaption if charged more than 100%
     * @throws BatteryException 
     */
    @Test(expected = BatteryException.class)
    public void charge() throws BatteryException {
        ElectricBattery bat = new ElectricBattery();
        for(int i=0;i<110;i++)
            bat.charge();

    }
}