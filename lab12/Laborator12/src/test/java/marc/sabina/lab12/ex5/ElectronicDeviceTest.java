package marc.sabina.lab12.ex5;

import org.junit.Test;

import static org.junit.Assert.*;

public class ElectronicDeviceTest {
    ElectronicDevice ed = new ElectronicDevice();

    @Test
    public void isPowered() {
        ed.turnOn();
        assertEquals(true, ed.isPowered());
    }


}