package marc.sabina.lab12.ex4;


import org.junit.Test;

import static org.junit.Assert.*;

public class AccountsManegerTest {

    @Test
    public void addAccount() {
        AccountsManeger am = new AccountsManeger();
        am.addAccount(new BankAccount("ABB",123));
        am.addAccount(new BankAccount("XYT",123));
        am.addAccount(new BankAccount("DSC",123));
        am.addAccount(new BankAccount("AST",123));
        assertEquals(4, am.count());
    }

    @Test
    public void findById() {
        AccountsManeger am = new AccountsManeger();
        am.addAccount(new BankAccount("ABB",123));
        am.addAccount(new BankAccount("XYT",123));
        am.addAccount(new BankAccount("DSC",123));
        am.addAccount(new BankAccount("AST",123));
        assertTrue(am.exists("ABB"));
        assertFalse(am.exists("GHTY"));
    }

}