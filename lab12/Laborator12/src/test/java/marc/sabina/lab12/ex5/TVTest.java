package marc.sabina.lab12.ex5;

import org.junit.Test;

import static org.junit.Assert.*;

public class TVTest {

    TV television = new TV();


    @Test
    public void channelUp() {
       television.setChannel(3);
       while(television.getChannel()<10) television.channelUp();
       assertEquals(10,television.getChannel());
    }

    @Test
    public void channelDown() {
        television.setChannel(15);
        while (television.getChannel()>2) television.channelDown();
        assertEquals(2,television.getChannel());
    }

    @Test
    public void getChannel() {
        television.setChannel(50);
        assertEquals(50,television.getChannel());
    }
}