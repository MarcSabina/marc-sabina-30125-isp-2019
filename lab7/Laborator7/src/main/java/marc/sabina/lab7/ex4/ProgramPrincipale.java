package marc.sabina.lab7.ex4;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import marc.sabina.lab7.ex4.*;
public class ProgramPrincipale {
    public static void main(String[] args) {
        String filename = "carrs.txt";
        Scanner k = new Scanner(System.in);
        ArrayList<Cars> c = new ArrayList<Cars>();
        while (true) {
            System.out.println("Meniu:\n");
            System.out.println("1.Create Objects\n");
            System.out.println("2.Save object\n");
            System.out.println("3.Show objects\n");
            System.out.println("9.Exit Program\n");
            int option = k.nextInt();
            {
                if (option == 1) {
                    c.add(createObject());
                }
                if (option == 2) {
                    try {
                        //Saving of object in a file
                        FileOutputStream file = new FileOutputStream(filename);
                        ObjectOutputStream out = new ObjectOutputStream(file);

                        // Method for serialization of object

                        out.writeObject(c);
                        out.close();


                        file.close();

                        System.out.println("Object has been serialized");

                    } catch (IOException ex) {
                        System.out.println("IOException is caught");
                    }
                } else if (option == 3) {
                    Cars c1 = null;
                    try {
                        // Reading the object from a file
                        FileInputStream file = new FileInputStream(filename);
                        ObjectInputStream in = new ObjectInputStream(file);

                        // Method for deserialization of object
                        c = (ArrayList<Cars>)in.readObject();

                        for (int i = 0; i < c.size(); i++) {
                            System.out.println(c.get(i).getModel());
                            System.out.println(c.get(i).getPrice());
                        }


                        in.close();
                        file.close();
                        System.out.println("Object has been deserialized ");
                        System.out.println("Model = " + c1.getModel());
                        System.out.println("Price = " + c1.getPrice());
                    } catch (IOException ex) {
                        System.out.println("IOException is caught");
                    } catch (ClassNotFoundException ex) {
                        System.out.println("ClassNotFoundException is caught");
                    }

                } else if (option == 9)
                    break;
            }
        }
    }

    public static Cars createObject() {
        Scanner k = new Scanner(System.in);
        String f = "carrs.txt";
        System.out.println("Introduceti modelul masinii:");
        String model = k.nextLine();
        System.out.println("Introduceti pretul masinii");
        double price = k.nextDouble();
        Cars c = new Cars(price, model);
        return c;
    }
}
