package marc.sabina.lab7.ex4;
import java.io.Serializable;

public class Cars  implements Serializable {
    private  String model;
    private double price;

    public Cars()
    {
        model="Default Model";
        price=0.0;
    }
    public Cars(double price,String model)
    {
        this.model=model;
        this.price=price;
    }
    public String getModel()
    {
        return model;
    }
    public double getPrice()
    {
        return price;
    }
}

