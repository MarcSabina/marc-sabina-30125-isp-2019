package marc.sabina.lab7.ex1;

class CoffeException extends Exception {
	int n;
    public CoffeException(int n,String msg) {
    	super(msg);
        this.n = n;
    }
    int getNumberofCoffes(){
        return n;
  }
}