package marc.sabina.lab7.ex3;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Encrypt {
	public static void main(String [] args) throws IOException{
		BufferedReader in1 = new BufferedReader(new FileReader("data.enc.txt"));
        String s, s1 = new String();
        while((s = in1.readLine())!= null)
          s1 += s + "\n";
        in1.close();
        BufferedReader in2 = new BufferedReader(new FileReader("data.dec.txt"));
        String x, s2 = new String();
        while((x = in2.readLine())!= null)
          s2 += x + "\n";
        in2.close();
        System.out.println("Encrypt(e)/Decrypt(d)?");
        Scanner sc = new Scanner(System.in);
    	char i = sc.next().charAt(0);
    	String n=new String();
    	if(i=='e')
    	{for (int index = 0; index < s1.length();
    			index++) {
    	     char a = s1.charAt(index);
    	     if(a!='\n'&&a!=' ')
    	    	 if(a=='z')
    	    		 a='a';
    	    	 else
    	    		 a++;
    	     n+=a;
    	}
    		System.out.println(n);
    	}
    	else if(i=='d') 
    	{
    		for (int index = 0; index < s2.length();
        			index++) {
        	     char a = s2.charAt(index);
        	     if(a!='\n'&&a!=' ')
        	    	 if(a=='a')
        	    		 a='z';
        	    	 else
        	    		 a--;
        	     n+=a;
    	}
    		System.out.println(n);
    	}
    	else System.out.println("Invalid Input");
        
	}
}