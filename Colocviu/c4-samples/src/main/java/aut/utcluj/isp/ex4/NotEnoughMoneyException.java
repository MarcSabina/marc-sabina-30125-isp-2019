package aut.utcluj.isp.ex4;

/**
 * @author stefan
 */
public class NotEnoughMoneyException extends Exception {
	int m;
    public NotEnoughMoneyException(int m,String msg) {
          super(msg);
          this.m = m;
    }

    int getMoney(){
          return m;
    }
}
