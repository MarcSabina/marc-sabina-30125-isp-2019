package aut.utcluj.isp.ex2;


/**
 * @author stefan
 */
public class Shop {
    private String name="";
    private String city="";

    public Shop(String name, String city) {
        this.name= name;
        this.city= city;
    }

    public Shop(String name) {
        this.name = name;
        this.city= "";
        
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }
    
    public boolean equals(Shop x){
        if(x.getName()==this.getName()&& x.getCity()==this.getCity())
        	return true;
        else
        	return false;
        
    } 
    @Override
	public String toString() {
    	///Shop: eMag City: Cluj
		return "Shop: " + this.getName() + " City: "+this.getCity();
	}
}