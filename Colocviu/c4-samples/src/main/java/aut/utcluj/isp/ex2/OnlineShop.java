package aut.utcluj.isp.ex2;


/**
 * @author stefan
 */
public class OnlineShop {
    private String webAddress;
    Shop shop=new Shop(null,null);
    public OnlineShop(String name, String city, String webAddress) {
        this.webAddress= webAddress;
        shop=new Shop(name,city);  
    }

    public String getWebAddress() {
        return webAddress;
    }
    
    public String toString() {
    	///Shop: eMag City: Cluj Web address: https://www.emag.ro
    	return shop.toString()+" Web address: "+this.getWebAddress();
	}
    
    public boolean instanceOf()
    {
    	return int.class.isAssignableFrom(Shop.class);
    }
}
