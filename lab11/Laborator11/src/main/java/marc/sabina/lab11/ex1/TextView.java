package marc.sabina.lab11.ex1;

import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class TextView extends JPanel implements Observer {
    JTextField tf;
    JLabel l;

    TextView() {
        this.setLayout(new FlowLayout());
        tf = new JTextField(10);
        l = new JLabel("Temperature:");
        add(tf);
        add(l);
    }

    public void update(Observable o, Object arg) {
        String s = "" + ((Sensor) o).getTemperature();
        tf.setText(s);
    }


}