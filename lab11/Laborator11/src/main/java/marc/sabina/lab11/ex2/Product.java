package marc.sabina.lab11.ex2;

public class Product {
	private String productName;
	private int quantity;
	private double price;
	
	public String getName() {
		return productName;
	}
	public void setName() {
		this.productName=productName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity() {
		this.quantity=quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice() {
		this.price=price;
	}
	public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Product)) {
            return false;
        }

        Product x = (Product) o;

        return x.productName.equals(productName);
    }
	public final int hashCode(){
		return (int) (productName.hashCode());
    }
	
}
