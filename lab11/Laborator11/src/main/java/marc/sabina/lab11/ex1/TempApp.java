package marc.sabina.lab11.ex1;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class TempApp extends JFrame {
    public TempApp(TextView t){
        setLayout(new BorderLayout());
        add(t,BorderLayout.SOUTH);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        Sensor ts=new Sensor();
        ts.start();

        TextView tv = new TextView();
        Controller tc = new Controller(ts,tv);

        new TempApp(tv);
    }
}