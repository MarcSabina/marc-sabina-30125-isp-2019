package marc.sabina.lab2.ex7;

import java.util.Scanner;
import java.util.Random;

class Guess_the_number_game {
	public static void main(String[] args){
		Scanner in = new Scanner(System.in);
		Random rand = new Random();
		int n_generat= rand.nextInt(100);
		int incercari=1,n_citit=0;
		while(incercari<=3)
		{ System.out.println("I"+incercari+":");
		  n_citit=in.nextInt();
		  incercari++;
		  if(n_citit>n_generat)
			  System.out.println("Raspuns gresit, numarul citit e prea mare");
		  else
			  if(n_citit<n_generat)
				  System.out.println("Raspuns gresit, numarul citit e prea mic");
			  else
			  {System.out.println("Raspuns corect!"); System.exit(0);}
		}
		
		System.out.println("Ati pierdut, numarul era "+n_generat);
		
	}
}
