package marc.sabina.lab10.ex4;

import java.util.Random;

public class Robot extends Thread{
	int x,y;
	String name;
	static Robot r[]=new Robot[10];
	Robot(int x,int y,String name){
		this.x=x;
		this.y=y;
		this.name=name;
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public String getRobot() {
		return name;
	}
	public void run() {
		while(true) {
		 for(int i=0;i<10;i++)
		 {	if(r[i]!=null) {Random rand = new Random();
		 	int ResultX = rand.nextInt(100);
		 	int ResultY = rand.nextInt(100);
			 r[i].setX(ResultX);
			 r[i].setY(ResultY);
		 }
		 }
		
		try {
			for(int i=0;i<9;i++) {
				for(int j=i+1;j<10;j++) {
					if(r[i]!=null&&r[j]!=null) {
					System.out.println("X:Compare "+r[i].getX()+"  with  "+r[j].getX());
					System.out.println("Y:Compare "+r[i].getY()+"  with  "+r[j].getY());
					}
					if(r[i]!=null&&r[j]!=null&&(r[i].getX()==r[j].getX())&&(r[i].getY()==r[j].getY())) {
							System.out.println(r[i].getRobot()+" and "+r[j].getRobot()+" were distroyed\n");
										r[i]=null;r[j]=null;
					}
				}
			}
	         Thread.sleep(100);
	   } catch (InterruptedException e) {
	         e.printStackTrace();
	   }
	}
	}
	public static void main(String[] args) {

		r[0]=new Robot(1,1,"Robot0");
		r[1]=new Robot(1,1,"Robot1");
		r[2]=new Robot(1,1,"Robot2");
		r[3]=new Robot(1,1,"Robot3");
		r[4]=new Robot(1,1,"Robot4");
		r[5]=new Robot(1,1,"Robot5");
		r[6]=new Robot(1,1,"Robot6");
		r[7]=new Robot(1,1,"Robot7");
		r[8]=new Robot(1,1,"Robot8");
		r[9]=new Robot(1,1,"Robot9");
		r[0].run();
		r[1].run();
		r[2].run();
		r[3].run();
		r[4].run();
		r[5].run();
		r[6].run();
		r[7].run();
		r[8].run();
		r[9].run();
	}

}	
