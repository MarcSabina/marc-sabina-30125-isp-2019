package marc.sabina.lab10.ex3;

class Counter2 extends Thread
{
      String n;
      Thread t;
      Counter2(String n, Thread t){this.n = n;this.t=t;}
 
      public void run()
      {int n1=0,n2=100,x=1;
    	          try
            {                
                  if (t!=null) { t.join();n1=100;n2=200;x=2;}
                  for(int i=n1;i<=n2;i++){
                      System.out.println(" i = "+i);}
                  Thread.sleep((int)(Math.random() * 1000));
                  System.out.println("Counter "+x+" finished it's job.");
                  
            }
            catch(Exception e){e.printStackTrace();} 
    	  }
 
      
 
public static void main(String[] args)
{
	Counter2 w1= new Counter2("Counter 1",null);
	Counter2 w2 = new Counter2("Counter 2",w1);
      w1.start();
      w2.start();
}
}
