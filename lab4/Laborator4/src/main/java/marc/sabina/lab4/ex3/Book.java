package marc.sabina.lab4.ex3;
import marc.sabina.lab4.ex2.Author;
public class Book {
	   // The private instance variables
	   private String name;
	   private Author author;
	   private double price;
	   private int qtyInStock;
	 
	   // Constructor
	   public Book(String name, Author a, double price, int qty) {
	      this.name = name;
	      this.author = a;
	      this.price = price;
	      this.qtyInStock = qty;
	   }
	 
	   // Getters and Setters
	   public String getName() {
	      return name;
	   }
	   public Author getAuthor() {
	      return author;  // return member author, which is an instance of the class Author
	   }
	   public double getPrice() {
	      return price;
	   }
	   public void setPrice(double price) {
	      this.price = price;
	   }
	   public int getQtyInStock() {
	      return qtyInStock;
	   }
	   public void setQtyInStock(int qty) {
	      this.qtyInStock = qty;
	   }
	 
	   // The toString() describes itself
	   public String toString() {
	      return  name + " by " + author;  // author.toString()
	   }
	}
