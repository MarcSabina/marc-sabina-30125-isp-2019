package marc.sabina.lab4.ex2;

public class Author {
	private char gender;
	private String name,email;
	public Author(char g,String n,String em) {
		gender=g;
		name=n;
		email=em;	
	}
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public char getGender() {
		return gender;
	}
	public void setEmail(String em_ex) {
		email=em_ex;
	}
	@Override
	public String toString() {
		return name+"("+gender+") at "+email;
	}
	
}
