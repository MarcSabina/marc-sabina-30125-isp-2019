package marc.sabina.lab4.ex4;
import marc.sabina.lab4.ex2.Author;
public class Book {
	   // The private instance variables
	   private String name;
	   private Author[] author;
	   private double price;
	   private int qtyInStock;
	 
	   // Constructor
	   public Book(String name, Author[] authors, double price, int qty) {
		      this.name = name;
		      this.author = authors;
		      this.price = price;
		      this.qtyInStock = qty;
		   }
	   // Getters and Setters
	   public String getName() {
	      return name;
	   }
	   public Author getAuthor(int n) {
	      return author[n];  // return member author, which is an instance of the class Author
	   }
	   public double getPrice() {
	      return price;
	   }
	   public void setPrice(double price) {
	      this.price = price;
	   }
	   public int getQtyInStock() {
	      return qtyInStock;
	   }
	   public void setQtyInStock(int qty) {
	      this.qtyInStock = qty;
	   }
	   public void printAuthors(int n) {
		   for(int i=1;i<=n;i++)
			   System.out.println(getAuthor(i));
	   }
	   // The toString() describes itself
	   public String toString() {
	      return  name + " by ";  // author.toString()
	   }
	}
