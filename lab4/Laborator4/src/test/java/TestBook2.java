import org.junit.Test;

import marc.sabina.lab4.ex2.Author;
import marc.sabina.lab4.ex4.Book;
import static org.junit.Assert.assertEquals;
/**
 * @Test
 */
public class TestBook2 {
	@Test
	public void testAdd() {
	 	int n=2;
	 // Declare and allocate an array of Authors
	 	Author[] authors = new Author[3];
	 	authors[1] = new Author('m',"Cartarescu","email@exe");
	 	authors[2] = new Author('m',"Barbu","email@exe");
	 // Declare and allocate a Book instance
	 	Book b = new Book("Solenoid", authors, 0, 0);
    	b.toString();
        assertEquals("Rez:", b.toString(),"Solenoid by ");
        b.printAuthors(n);   
    }
}