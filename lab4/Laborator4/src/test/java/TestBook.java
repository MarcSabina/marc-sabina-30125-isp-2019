import org.junit.Test;

import marc.sabina.lab4.ex2.Author;
import marc.sabina.lab4.ex3.Book;
import static org.junit.Assert.assertEquals;
/**
 * @Test
 */
public class TestBook {
	@Test
	public void testAdd() {
	 	Author a=new Author('m',"Cartarescu","email@exe");
    	Book b=new Book("Solenoid",a,0,0);
    	b.toString();
        assertEquals("Rez:", b.toString(),"Solenoid by Cartarescu(m) at email@exe");
    }
}