package marc.sabina.lab9.ex4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JToggleButton;

public class TicTacToe extends JFrame {
	String start="X";
    JToggleButton b11;
    JToggleButton b12;
    JToggleButton b13;
    JToggleButton b21;
    JToggleButton b22;
    JToggleButton b23;
    JToggleButton b31;
    JToggleButton b32;
    JToggleButton b33;
    TicTacToe(){
    setTitle("Tic-Tac-Toe");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    init();
    setSize(400,400);
    setVisible(true);
    }
    public void init(){
    	int turn=0;
         this.setLayout(null);
            int width=80;int height = 20;
            b11=new JToggleButton("");
            b11.setBounds(10, 10, 90, 90);
            b11.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					b11.setText(start);
					b11.setEnabled(false);
					choosePlayer();
					stopGame();
					
				}
            	
            });
            b12=new JToggleButton("");
            b12.setBounds(10, 110, 90, 90);
            b12.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					b12.setText(start);
					b12.setEnabled(false);
					choosePlayer();
					stopGame();
					
				}
            	
            });
            b13=new JToggleButton("");
            b13.setBounds(10, 210, 90, 90);
            b13.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					b13.setText(start);
					b13.setEnabled(false);
					choosePlayer();
					stopGame();
					
				}
            	
            });
            b21=new JToggleButton("");
            b21.setBounds(110, 10, 90, 90);
            b21.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					b21.setText(start);
					b21.setEnabled(false);
					choosePlayer();
					stopGame();
					
				}
            	
            });
            b22=new JToggleButton("");
            b22.setBounds(110, 110, 90, 90);
            b22.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					b22.setText(start);
					b22.setEnabled(false);
					choosePlayer();
					stopGame();
					
				}
            	
            });
            b23=new JToggleButton("");
            b23.setBounds(110, 210, 90, 90);
            b23.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					b23.setText(start);
					b23.setEnabled(false);
					choosePlayer();
					stopGame();
					
				}
            	
            });
            b31=new JToggleButton("");
            b31.setBounds(210, 10, 90, 90);
            b31.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					b31.setText(start);
					b31.setEnabled(false);
					choosePlayer();
					stopGame();
				}
            	
            });
            b32=new JToggleButton("");
            b32.setBounds(210, 110, 90, 90);
            b32.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					b32.setText(start);
					b32.setEnabled(false);
					choosePlayer();
					stopGame();
					
				}
            	
            });
            b33=new JToggleButton("");
            b33.setBounds(210, 210, 90, 90);
            b33.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					b33.setText(start);
					b33.setEnabled(false);
					choosePlayer();
					stopGame();
					
				}
            	
            });
            add(b11);
            add(b12);
            add(b13);
            add(b21);
            add(b22);
            add(b23);
            add(b31);
            add(b32);
            add(b33);
            
    }
    public static void main(String[]args){
        new TicTacToe();
    }
    public void choosePlayer() {
    	if(start.equalsIgnoreCase("X")) {
    		start="O";
    	}
    	else {
    		start="X";
    	}
    	
    }
    public void stopGame() {
    	if (b11.isSelected() && b12.isSelected() && b13.isSelected() && b11.getText()==b12.getText() && b11.getText()==b13.getText())
    		{b21.setEnabled(false); b22.setEnabled(false); b23.setEnabled(false); b31.setEnabled(false); b32.setEnabled(false); b33.setEnabled(false);
    		getWinner(b11);}
    	if (b21.isSelected() && b22.isSelected() && b23.isSelected()&& b21.getText()==b22.getText() && b21.getText()==b23.getText())
    		{b11.setEnabled(false); b12.setEnabled(false); b13.setEnabled(false); b31.setEnabled(false); b32.setEnabled(false); b33.setEnabled(false);
    		getWinner(b21);}
    	if (b31.isSelected() && b32.isSelected() && b33.isSelected() && b31.getText()==b32.getText() && b31.getText()==b33.getText())
    		{b11.setEnabled(false); b12.setEnabled(false); b13.setEnabled(false); b21.setEnabled(false); b22.setEnabled(false); b23.setEnabled(false);
    		getWinner(b11);}
    	if (b11.isSelected() && b21.isSelected() && b31.isSelected() && b11.getText()==b21.getText() && b11.getText()==b31.getText())
    		{b22.setEnabled(false); b12.setEnabled(false); b13.setEnabled(false); b23.setEnabled(false); b32.setEnabled(false); b33.setEnabled(false);
    		getWinner(b11);}
    	if (b12.isSelected() && b22.isSelected() && b32.isSelected() && b12.getText()==b22.getText() && b12.getText()==b32.getText())
    		{b11.setEnabled(false); b21.setEnabled(false); b13.setEnabled(false); b23.setEnabled(false); b31.setEnabled(false); b33.setEnabled(false);
    		getWinner(b12);}
    	if (b13.isSelected() && b23.isSelected() && b33.isSelected() && b13.getText()==b23.getText() && b13.getText()==b33.getText())
    		{b22.setEnabled(false); b12.setEnabled(false); b11.setEnabled(false); b21.setEnabled(false); b32.setEnabled(false); b31.setEnabled(false);
    		getWinner(b13);}
    	if (b11.isSelected() && b22.isSelected() && b33.isSelected() && b11.getText()==b22.getText() && b11.getText()==b33.getText())
    		{b21.setEnabled(false); b12.setEnabled(false); b13.setEnabled(false); b23.setEnabled(false); b32.setEnabled(false); b31.setEnabled(false);
    		getWinner(b11);}
    	if (b31.isSelected() && b22.isSelected() && b13.isSelected()&& b31.getText()==b22.getText() && b31.getText()==b13.getText())
    		{b21.setEnabled(false); b12.setEnabled(false); b11.setEnabled(false); b23.setEnabled(false); b32.setEnabled(false); b33.setEnabled(false);
    		getWinner(b31);}

		
    }
    
    public void getWinner(JToggleButton b) {
    	String w1="X";
    	if(w1.equals(b.getText()))
    			setTitle("Winner is X");
    		else
    			setTitle("Winner is O");
    			
    }
  
}


