package marc.sabina.lab9.ex2;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class CountButtonApp extends JFrame {
	JTextField tf;
	JButton b; 
	private int cnt=0;
	CountButtonApp(){
		 
        setTitle("Press The Button");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300,350);
        setVisible(true);
  }
	 public void init(){
		 
         this.setLayout(null);
         int width=200; int height = 70;
         tf = new JTextField("\t0");
         tf.setBounds(50,50,width, height);
         b = new JButton("Count");
         b.setBounds(50,180,width, height);
         b.addActionListener(new TratareButon());
         add(tf);
         add(b);

   }
	 public static void main(String[] args) {
		 new CountButtonApp();
   }

   class TratareButon implements ActionListener{
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		  ++cnt;                     // Incrase the counter value
	      tf.setText("\t"+cnt);
	}

   }

}
