package marc.sabina.lab9.ex3;

import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import marc.sabina.lab9.ex1.ButtonAndTextField2;
import marc.sabina.lab9.ex2.CountButtonApp;

public class ShowTheText extends JFrame {

	JButton b; 
	JTextField tf;
	JLabel f;
	JTextArea content;
	ShowTheText(){
        setTitle("Show The Text");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500,650);
        setVisible(true);
  }
	public void init(){
		 
        this.setLayout(null);
        int width=200; int height = 70;
        f = new JLabel("File Name: ");
        f.setBounds(10, 50, width, height);
        tf = new JTextField();
        tf.setBounds(80,50,width, height);
        b = new JButton("Ok");
        width=70;
        height=20;
        b.setBounds(130,140,width, height);
        b.addActionListener(new TratareButon());
        width=200; height = 30;
        content = new JTextArea();
        content.setBounds(60,180,370,400);
        add(f);
        add(tf);
        add(b);
        add(content);

  }
	public static void main(String[] args) {
		 new ShowTheText();
  }
	
	class TratareButon implements ActionListener{
			 
            public void actionPerformed(ActionEvent e) {
 
                  String file = ShowTheText.this.tf.getText(); 
                  String filename="C:\\Users\\Denisa\\marc-sabina-30125-isp-2019\\lab9\\Laborator9\\src\\main\\java\\marc\\sabina\\lab9\\ex3\\"+file+".txt";
                  String encoding = "Cp1250";
                  File file1 = new File(filename);
                  if (file1.exists()) {
                      try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file1), encoding))) {
                          String line = null;
                          while ((line = br.readLine()) != null) {
                        	  ShowTheText.this.content.append(line+'\n');
                          }
                      } catch (IOException e1) {
                          e1.printStackTrace();
                      }
                  }
                  else {
                	  			ShowTheText.this.content.append(file+" is not found");
                  }
            }
	}
}
