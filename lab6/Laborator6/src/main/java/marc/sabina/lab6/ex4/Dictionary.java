package marc.sabina.lab6.ex4;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

public class Dictionary {
    HashMap dictionary = new HashMap();

    public void addWord(Word w, Definition d) {

        if (dictionary.containsKey(w))
            System.out.println("cuvant existent!");
        else
            System.out.println("Adauga cuvant nou.");
        dictionary.put(w, d);
    }

    public Definition getDefinition(Word w) {
       
            return (Definition) dictionary.get(w);
    }

    public Set getAllWords() {
        return  dictionary.keySet();
    }

    public Collection getAllDefinitions() {
        return dictionary.values();
    }


    public String cautaCuvant(Word c) {
        System.out.println("Cauta " + c);
        System.out.println(dictionary.containsKey(c));
        return (String) dictionary.get(c);
    }


}
