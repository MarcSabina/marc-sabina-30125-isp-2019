package marc.sabina.lab6.ex4;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ConsoleMenu {
    public static void main(String args[]) throws Exception {
        Dictionary dict = new Dictionary();
        char raspuns;
        String linie;
        String explic;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));
        do {
            System.out.println("Meniu");
            System.out.println("a - Adauga cuvant");
            System.out.println("c - obtine definitie");
            System.out.println("w - Listeaza toate cuvintele");
            System.out.println("d - Listeaza toate definitiile");
            System.out.println("e  - exit");

            linie = fluxIn.readLine();
            raspuns = linie.charAt(0);

            switch (raspuns) {
                case 'a':
                case 'A':
                    System.out.println("Introduceti cuvantul:");
                    linie = fluxIn.readLine();
                    if (linie.length() > 1) {
                        System.out.println("Introduceti definitia:");
                        explic = fluxIn.readLine();
                        dict.addWord(new Word(linie), new Definition(explic));
                    }
                    break;
                case 'c':
                case 'C':
                    System.out.println("Obtine definitia cuvantului cautat:");
                    linie = fluxIn.readLine();

                    if (linie.length() > 1) {
                        Word x = new Word(linie);
                        System.out.println(dict.getDefinition(x));
                    }
                    break;
                case 'w':
                case 'W':
                    System.out.println("Afiseaza toate cuvintele:");
                    System.out.println(dict.getAllWords());
                    break;
                case 'd':
                case 'D':
                    System.out.println("Afiseaza toate defintiiile:");
                    System.out.println(dict.getAllDefinitions());
                    break;

            }
        }
        while (raspuns != 'e' && raspuns != 'E');
        System.out.println("Program terminat.");
    }
}
