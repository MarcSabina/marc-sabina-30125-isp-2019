package marc.sabina.lab6.ex2;

public class BankAccount {
	private String owner;
	private double balance;
	
	public BankAccount( double balance, String owner) {
		this.owner = owner;
		this.balance = balance;
		   }
	public void withdraw(double amount)
	{
	    if (amount > 0.0)
	        if (amount > balance)
	            System.out.println("Withdrawal amount exceeded account balance");
	        else
	            balance = balance - amount;
	}
	public void deposit(double amount){
	 balance += amount;
	}
	@Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof BankAccount)) {
            return false;
        }

        BankAccount user = (BankAccount) o;

        return user.owner.equals(owner) &&
                user.balance == balance ;
    }
	
	public final int hashCode(){
		return (int) (owner.hashCode() + balance);
    }
}
