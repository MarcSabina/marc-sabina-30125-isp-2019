package marc.sabina.lab6.ex3;

import java.util.Comparator;

public class BankAccount implements Comparable<BankAccount> {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount) {
        balance -= amount;
    }

    public void deposit(double amount) {
        balance += amount;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount ba = (BankAccount) obj;
            return balance == ba.balance && ba.owner.equals(owner);
        }

        return false;
    }

    @Override
    public String toString() {
        return this.owner + " " + this.balance;
    }

    @Override
    public int hashCode() {
        return owner.hashCode() + (int) balance;
    }

    public int compareTo(BankAccount o) {
        return (int) this.balance - (int) o.balance;
    }

    public static Comparator<BankAccount> CompareByOwner = new Comparator<BankAccount>() {
        public int compare(BankAccount o1, BankAccount o2) {
            return o1.getOwner().compareTo(o2.getOwner());
        }
    };
}

