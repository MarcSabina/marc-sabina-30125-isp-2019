package marc.sabina.lab6.ex2;

import static marc.sabina.lab6.ex3.BankAccount.CompareByOwner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.TreeSet;

import marc.sabina.lab6.ex3.BankAccount;

public class Bank {
	  private int balance;
	    private String owner;
	    private ArrayList<String> accounts = new ArrayList<String>();
	    private ArrayList<BankAccount> accounts1 = new ArrayList<BankAccount>();
	    public ArrayList<BankAccount> getAccounts() {
	        return accounts1;
	    }


	    public void addAcount(String owner, double balance) {
	        BankAccount b = new BankAccount(owner, balance);
	        accounts.addAll(b);
	        accounts1.add(b);
	    }

	    public void printAccounts() {
	        System.out.println("Conturile in ordine crescatoare dupa balance:");
	        Collections.sort(accounts);
	        Iterator<String> i = accounts.iterator();
	        while (i.hasNext()) {
	            String acc = i.next();
	           System.out.println(acc.toString());
	        System.out.println(accounts);
	    }
	    }
}
