package marc.sabina.lab6.ex4;

public class Definition {
    private String description;

    public Definition(String description) {
        this.description = description;
    }


    @Override
    public int hashCode() {
        return (int) (description.length() * 1000);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Definition))
            return false;
        Definition x = (Definition) obj;
        return description.equals(x.description);
    }

    @Override
    public String toString() {
        return description;
    }

}
