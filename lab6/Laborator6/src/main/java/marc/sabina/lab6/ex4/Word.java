package marc.sabina.lab6.ex4;

public class Word {
    String name;

       public Word(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Word))
            return false;
        Word x = (Word) obj;
        return name.equals(x.name);
    }

    @Override
    public int hashCode() {
        return (int) (name.length() * 1000);
    }

    public String toString() {
        return name;
    }
}
