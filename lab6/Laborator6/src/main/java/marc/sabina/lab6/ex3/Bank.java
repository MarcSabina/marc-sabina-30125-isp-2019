package marc.sabina.lab6.ex3;

import java.util.TreeSet;

import static marc.sabina.lab6.ex3.BankAccount.CompareByOwner;

public class Bank {
    private int balance;
    private String owner;

    private TreeSet<BankAccount> accounts = new TreeSet<BankAccount>();
    private TreeSet<BankAccount> accounts1 = new TreeSet<BankAccount>(CompareByOwner);
    public TreeSet<BankAccount> getAccounts() {
        return accounts1;
    }


    public void addAcount(String owner, double balance) {
        BankAccount b = new BankAccount(owner, balance);
        accounts.add(b);
        accounts1.add(b);
    }

    public void printAccounts() {
        System.out.println("Conturile in ordine crescatoare dupa balance:");
        //Collections.sort(accounts);
        // Iterator<BankAccount> i = accounts.iterator();
        // while (i.hasNext()) {
        //    BankAccount acc = i.next();
        //   System.out.println(acc.toString());
        System.out.println(accounts);
    }

}
