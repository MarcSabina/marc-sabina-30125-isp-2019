package marc.sabina.lab8.ex4;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

abstract class Home {
    private ControlUnit controlUnit;
    private final int SIMULATION_STEPS = 20;

    protected abstract void setValueInEnvironment(Event event);

    protected abstract void controllStep();

    public void simulate() {
        int k = 0;
        controlUnit = ControlUnit.getInstance();
        try {
            PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream("C:\\Users\\Denisa\\marc-sabina-30125-isp-2019\\lab8\\Laborator8\\system_logs")));
            System.setOut(out);
            while (k < SIMULATION_STEPS) {
                Event event = controlUnit.control();
                setValueInEnvironment(event);
                controllStep();


                try {
                    Thread.sleep(300);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }

                k++;
            }
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}
