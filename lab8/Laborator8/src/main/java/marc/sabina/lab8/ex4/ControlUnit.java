package marc.sabina.lab8.ex4;

import java.util.ArrayList;

public class ControlUnit {
    private static ControlUnit ourInstance = new ControlUnit();
    private ArrayList<FireSensor> fireSensors = new ArrayList<FireSensor>();
    private AlarmUnit alarmUnit;
    private GsmUnit gsmUnit;
    private HeatingUnit heatingUnit;
    private CoolingUnit coolingUnit;
    private TemperatureSensor temperatureSensor;


    public static ControlUnit getInstance() {

        return ourInstance;
    }

    public void addFireSensor() {
        FireSensor fire = new FireSensor();
        fireSensors.add(fire);
    }

    private ControlUnit() {
        gsmUnit = new GsmUnit("0729165983");
        alarmUnit = new AlarmUnit(false);
        addFireSensor();
        temperatureSensor = new TemperatureSensor();
        heatingUnit = new HeatingUnit(temperatureSensor);
        coolingUnit = new CoolingUnit(temperatureSensor);
    }

    public Event control() {
        for (FireSensor fire : fireSensors) {
            if (fire.isSmoke()) {
                alarmUnit.setAlarm(true);
                gsmUnit.toString();
                System.out.println("Fire started");
                System.out.println("Alarm started");
                return new FireEvent(true);

            }
        }

        if (heatingUnit.heating()) {
            System.out.println("Heating started");
            return new TemperatureEvent(temperatureSensor.getTemperature());
        }

        if (coolingUnit.cooling()) {
            System.out.println("Cooling started");
            return new TemperatureEvent(temperatureSensor.getTemperature());
        }


        return new NoEvent();


    }

}