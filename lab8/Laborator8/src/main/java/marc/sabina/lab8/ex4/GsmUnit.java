package marc.sabina.lab8.ex4;

public class GsmUnit {
    public String numberowner;


    public GsmUnit(String numberowner) {
        this.numberowner = numberowner;
    }

    public void setNumberowner(String numberowner) {
        this.numberowner = numberowner;
    }

    @Override
    public String toString() {
        return "Call owner" + numberowner;
    }
}