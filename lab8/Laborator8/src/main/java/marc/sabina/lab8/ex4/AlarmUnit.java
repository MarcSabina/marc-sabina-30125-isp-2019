package marc.sabina.lab8.ex4;

import marc.sabina.lab8.*;

@SuppressWarnings("unused")
public class AlarmUnit {
    public boolean alarm;

    public AlarmUnit(boolean alarm){
       this.alarm=alarm;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }
}
