package marc.sabina.lab8.ex4;

import java.util.Random;

public class CoolingUnit {

    public TemperatureSensor temperatureSensor;
    private Random r = new Random();

    public CoolingUnit(TemperatureSensor temperatureSensor2) {
		// TODO Auto-generated constructor stub
	}

	public boolean cooling() {
        if (temperatureSensor.getTemperature() > TemperatureLimit.HIGHTEMPERATURE) {
            temperatureSensor.setTemperature(r.nextInt(3) + 18);
            return true;
        } else
            return false;

    }
}
