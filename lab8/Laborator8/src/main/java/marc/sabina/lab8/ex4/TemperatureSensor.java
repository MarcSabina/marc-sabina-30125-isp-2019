package marc.sabina.lab8.ex4;


import java.util.Random;

public class TemperatureSensor {
    private int temperature;
    private Random r = new Random();


    public TemperatureSensor() {
        temperature = r.nextInt();
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "Temperature is:" + temperature;
    }
}
