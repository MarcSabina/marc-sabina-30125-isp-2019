package marc.sabina.lab8.ex4;

import java.util.Random;

public class FireSensor {
    private boolean smoke;
    private boolean fire;
    private Random r = new Random();


    public FireSensor() {
        fire = r.nextBoolean();
        smoke = fire;
    }

    public boolean getFire() {
        return fire;
    }


    public boolean isSmoke() {
        return smoke;
    }


    @Override
    public String toString() {
        if (isSmoke())
            return "In house is smoke";
        else
            return "House does not have smoke";
    }
}