package marc.sabina.lab3.ex2;

public class Circle {
	private double radius=1.0;
    private String color="red";
	 public Circle(double r,String c)
	    {
	        color=c;
	        radius=r;
	    }
	 public double getRadius() {
		 return radius;
	 }
	 public double getArea(double radius)
	 {
		 return Math.PI*(radius*radius);
	 }
}
