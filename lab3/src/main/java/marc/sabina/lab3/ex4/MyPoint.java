package marc.sabina.lab3.ex4;

public class MyPoint {
	int x,y;
	public MyPoint(){
        this(0, 0);}
	public MyPoint(int x, int y){
        this.x = x;
        this.y = y;
    }
	public void setX (int x){
        this.x = x;
    }

    public int getX(){
        return x;
    }

    public void setY (int y){
        this.y = y;
    }

    public int getY(){
        return y;
    }
    public void setXY (int x,int y){
        this.y = y;
        this.x = x;
    }
    @Override
	public String toString() {
		return "P("+x+","+y+")";
	}
    public double distance(int x, int y) {
        int xD = this.x - x;
        int yD = this.y - y;
        return Math.sqrt(xD * xD + yD * yD);
    }
    public double distance(MyPoint another){
    	        int xD = this.x - x;
    	        int yD = this.y - y;
    	        return Math.sqrt(xD*xD + yD*yD);
    	    }

}
