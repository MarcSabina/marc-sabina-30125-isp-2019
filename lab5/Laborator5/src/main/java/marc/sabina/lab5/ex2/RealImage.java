package marc.sabina.lab5.ex2;
import marc.sabina.lab5.ex2.Image;
public class RealImage implements Image {
	 
	   private String fileName;
	 
	   public RealImage(String fileName){
	      this.fileName = fileName;
	      loadFromDisk(fileName);
	   }
	   public void display() {
	      System.out.println("Displaying " + fileName);
	   }
	   private void loadFromDisk(String fileName){
	      System.out.println("Loading " + fileName);
	   }
	   public void RotatedImage() {
		   System.out.println("Display rotated " + fileName);
	   }
	}