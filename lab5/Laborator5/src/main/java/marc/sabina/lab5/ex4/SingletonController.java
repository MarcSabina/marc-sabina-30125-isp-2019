package marc.sabina.lab5.ex4;

import marc.sabina.lab5.ex3.*;

public final class SingletonController {

    private static final SingletonController ctrl = new SingletonController();
    private LightSensor l = new LightSensor(3);
    private TemperatureSensor t = new TemperatureSensor(4);

    private SingletonController() {
    }

    public static SingletonController getInstance() {
        return ctrl;
    }

    public void control() {

        System.out.println("Temperature + light values: "
                + t.readValue() + " " + l.readValue());

    }
}