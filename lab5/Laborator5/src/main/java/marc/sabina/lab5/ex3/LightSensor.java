package marc.sabina.lab5.ex3;

public class LightSensor extends Sensor {
    private int lightSensor;

    public LightSensor(int lightSensor) {
        this.lightSensor = lightSensor;
    }

    public void setLightSensor(int lightSensor) {
        this.lightSensor = lightSensor;
    }

    public int readValue() {
        return lightSensor;
    }
}
