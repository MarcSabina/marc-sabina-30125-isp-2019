package marc.sabina.lab5.ex3;

public class Controller {

    private LightSensor l;
    private TemperatureSensor t;

    public Controller(LightSensor l, TemperatureSensor t) {
        this.l = l;
        this.t = t;
    }

    public void control() {

        System.out.println("Temperature + light values: "
                + t.readValue() + " " + l.readValue());

}
}

