package marc.sabina.lab5.ex3;

public class TemperatureSensor extends Sensor {
    public int tempSensor;

    public TemperatureSensor(int tempSensor) {
        this.tempSensor = tempSensor;
    }

    public void setTempSensor(int tempSensor) {
        this.tempSensor = tempSensor;
    }

    public int readValue() {
        return tempSensor;
    }


}