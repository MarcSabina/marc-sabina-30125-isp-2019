import marc.sabina.lab5.ex1.Circle;
import marc.sabina.lab5.ex1.Rectangle;
import marc.sabina.lab5.ex1.Square;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
/**
 * @Test
 */
public class TestShapes {
	@Test
	public void testAdd() {
	 	Square s=new Square(3);
	 	Circle c=new Circle(2);
	 	Rectangle r=new Rectangle(3,4);
	 	c.getArea();
        assertEquals("Circle Area:", c.getArea(),4*Math.PI,0.0);
        r.getArea();
        assertEquals("Rectangle Area:", r.getArea(),12,0.0);
        s.getArea();
        assertEquals("Square Area:", s.getArea(),9,0.0);
    }
}