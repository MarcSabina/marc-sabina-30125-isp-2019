import marc.sabina.lab5.ex4.*;
import marc.sabina.lab5.ex3.LightSensor;
import marc.sabina.lab5.ex3.TemperatureSensor;

import java.util.Random;

public class TestSingleton {

    public static void main(String[] args) throws InterruptedException {
        LightSensor l = new LightSensor(1);
        TemperatureSensor t = new TemperatureSensor(2);
        SingletonController ctrl = SingletonController.getInstance();
       // System.out.println(ctrl);
        ctrl.control();
        Random r = new Random();
        int timp = 1;
        while (timp < 20) {
            l.setLightSensor(r.nextInt(50));
            t.setTempSensor(r.nextInt(27));
            ctrl.control();
            timp++;
            Thread.sleep(1000);
        }

    }
}

