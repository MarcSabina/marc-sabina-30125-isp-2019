
import java.util.Random;
import marc.sabina.lab5.ex3.*;
public class TestSensor {
    public static void main(String[] args) throws InterruptedException {
        LightSensor l = new LightSensor(1);
        TemperatureSensor t = new TemperatureSensor(2);
        Controller c1 = new Controller(l, t);

        Random r = new Random();
        int timp = 1;
        while (timp < 20) {
            l.setLightSensor(r.nextInt(100));
            t.setTempSensor(r.nextInt(100));
            c1.control();
            timp++;
            Thread.sleep(1000);
        }

    }
}