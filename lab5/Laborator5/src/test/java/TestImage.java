import static org.junit.Assert.assertEquals;

import org.junit.Test;

import marc.sabina.lab5.ex2.ProxyImage;
import marc.sabina.lab5.ex2.RealImage;
import marc.sabina.lab5.ex2.Image;

/**
 * @Test
 */
@SuppressWarnings("unused")
public class TestImage {
	@Test
	public void testAdd() {
	 	RealImage ri=new RealImage("img1");
	 	ProxyImage pri=new ProxyImage("img2");
	 	Image i1=ri;
	 	Image i2=pri;
	 	i1.display();
	 	i2.display();
    }
}